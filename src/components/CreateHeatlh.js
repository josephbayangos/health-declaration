import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import {
    Box,
    Heading,
    FormControl,
    FormLabel,
    Input,
    Container,
    Button,
} from '@chakra-ui/react';

function CreateHeatlh() {
    const [healthState, setHealthState] = useState({
        fullname: '',
        temperature: '',
        email: '',
        phonenumber: '',
    });

    const handleOnChange = e => {
        setHealthState({
            ...healthState,
            [e.target.name]: e.target.value,
        });
    };

    const onSubmit = e => {
        e.preventDefault();
        axios
            .post('http://localhost:5000/health/add', healthState)
            .then(res => (window.location = '/'))
            .catch(err => console.log('Error:' + err));
    };

    return (
        <Box>
            <Container maxW="80%" mt={10}>
                <Heading mb={4}>Create Health</Heading>
                <form onSubmit={onSubmit}>
                    <FormControl id="email" w="50%">
                        <FormLabel>Full Name</FormLabel>
                        <Input
                            name="fullname"
                            type="text"
                            value={healthState.fullname}
                            onChange={handleOnChange}
                        />
                        <FormLabel>Temperature</FormLabel>
                        <Input
                            name="temperature"
                            type="number"
                            value={healthState.temperature}
                            onChange={handleOnChange}
                        />
                        <FormLabel>Email</FormLabel>
                        <Input
                            name="email"
                            type="email"
                            value={healthState.email}
                            onChange={handleOnChange}
                        />
                        <FormLabel>Phone Number</FormLabel>
                        <Input
                            name="phonenumber"
                            type="text"
                            value={healthState.phonenumber}
                            onChange={handleOnChange}
                        />
                        <Button mt={4} type="submit">
                            Submit
                        </Button>
                    </FormControl>
                </form>
            </Container>
        </Box>
    );
}

export default CreateHeatlh;
