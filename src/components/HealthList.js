import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Box, Button, Heading, Text } from '@chakra-ui/react';

function HealthList() {
    const [health, setHealth] = useState([]);

    const deleteHealth = id => {
        axios
            .delete('http://localhost:5000/health/' + id)
            .then(res => console.log(res.data));
        setHealth(health.filter(data => data._id !== id));
    };

    //get data
    useEffect(() => {
        axios
            .get('http://localhost:5000/health/')
            .then(res => {
                setHealth(res.data);
            })
            .catch(error => console.log(error));
    }, []);

    return (
        <Box mx="auto" w="80%">
            <Heading my={4}>Health List</Heading>

            {health.map(healthData => (
                <Box
                    border="1px solid #DCDCDC"
                    p={8}
                    rounded={8}
                    mb={4}
                    w="45%"
                    key={healthData._id}
                >
                    <Text>Full Name: {healthData.fullname}</Text>
                    <Text>Temperature: {healthData.temperature}</Text>
                    <Text>Email: {healthData.email}</Text>
                    <Text>Phone Number: {healthData.phonenumber}</Text>
                    <Link to={'/edit/' + healthData._id}>
                        <Button mr={2}>Edit</Button>
                    </Link>

                    <Button onClick={() => deleteHealth(healthData._id)}>
                        Delete
                    </Button>
                </Box>
            ))}
        </Box>
    );
}

export default HealthList;
