import { Box } from '@chakra-ui/react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import CreateHeatlh from './components/CreateHeatlh';
import EditHealth from './components/EditHealth';
import HealthList from './components/HealthList';

import NavbarHeader from './components/NavbarHeader';

function App() {
    return (
        <Router>
            <NavbarHeader />
            <Route path="/" exact component={HealthList} />
            <Route path="/edit/:id" component={EditHealth} />
            <Route path="/create" component={CreateHeatlh} />
        </Router>
    );
}

export default App;
